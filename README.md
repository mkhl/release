# Release

tools and resources for releasing software

requires only POSIX tools,
assumes changelogs based on [keep a changelog](https://keepachangelog.com/en/1.0.0/)

## scripts

* `version <version>`: update the changelog for the release of `<version>`
* `changes <version>`: print the changelog entries for `<version>`

## usage

### `version`

with [cargo-release]:

    [package.metadata.release]
    pre-release-hook = ["bin/version", "{{version}}"]

with npm or yarn:

	"scripts": {
		"version": "bin/version $npm_package_version && git add CHANGELOG.md"
	}

[cargo-release]: https://github.com/crate-ci/cargo-release

### `changes`

with gitea releases:

	tea releases create \
		--tag "v$VERSION" \
		--target "v$VERSION" \
		--title "$VERSION" \
		--note "$(bin/changes "$VERSION" | sed 1d)"
